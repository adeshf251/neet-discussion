package com.awesomeinnovators.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ScrollView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MessageBoardActivity extends AppCompatActivity {

    MessageAdaper messageAdaper;
    RecyclerView recyclerView;
    List<Message> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_board);

        updateRecycleView();
        list = getData();

//        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
//        messageAdaper = new MessageAdaper(list, getApplication());
//        recyclerView.setAdapter(messageAdaper);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        updateRecycleView();

    }

    private void updateRecycleView() {
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        messageAdaper = new MessageAdaper(list, getApplication());
        messageAdaper.notifyDataSetChanged();
        recyclerView.setAdapter(messageAdaper);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.scrollToPosition(list.size()-1);



//        final ScrollView scroll = (ScrollView)findViewById(R.id.message_scroll_board);
//        scroll.post(new Runnable() {
//            @Override
//            public void run() {
//                scroll.fullScroll(View.FOCUS_DOWN);
//            }
//        });
    }

    private List<Message> getData()
    {
        List<Message> list = new ArrayList<>();
        list.add(new Message("Message A 1 ", "message id here ....asasa", false, true));
        list.add(new Message("Message B 2", "message id here ....asasa", false, true));
        list.add(new Message("Message C 3", "message id here ....asasa", false, true));
        list.add(new Message("Message D 4", "message id here ....asasa", false, true));
        list.add(new Message("Message E 5", "message id here ....asasa", true, false));
        list.add(new Message("Message F 6", "message id here ....asasa", true, false));
        list.add(new Message("Message G 7", "message id here ....asasa", false, true));
        list.add(new Message("Message H 8", "message id here ....asasa", false, true));
        list.add(new Message("Message I 9", "message id here ....asasa", false, true));
        list.add(new Message("Message J 10", "message id here ....asasa", true, false));
        list.add(new Message("Message K 11", "message id here ....asasa", false, true));

        return list;
    }

    public void sendMessage(View view) {
        Toast.makeText(this, "sendMessage Clicked", Toast.LENGTH_SHORT).show();
        list.add(new Message("Message 11 ", "message id here ....asasa", false, true));

        updateRecycleView();
    }
}
