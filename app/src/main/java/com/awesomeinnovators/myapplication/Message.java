package com.awesomeinnovators.myapplication;

import android.util.Log;

public class Message {

    private String msgId;
    private String replyId;
    private String msg;
    private boolean myMsg;
    private boolean theirMsg;
    private String TAG = "Message Structure Class";

    public Message(String message, String messageID, boolean is_Sender, boolean is_Reciever) {
        msgId = messageID;
        replyId = null;
        msg = message;
        myMsg = is_Sender;
        theirMsg = is_Reciever;
        Log.i(TAG, "Added for msg :" + msg);
    }

    public String getMessage() {
        return msg;
    }

    public String getMsgId() {
        return msgId;
    }

    public boolean isMyMsg() {
        return myMsg;
    }

    public boolean isTheirMsg() {
        return theirMsg;
    }

    public void setReplyID(String replyID) {
        replyId = replyID;
    }

    public String getReplyId() {
        return replyId;
    }

    public String getReplyMsgContent() {
        /*
          To be implemented later
          1- Use local sqlite
          2- if not found locally than request server for content
          3- if above fails .... than "reply protected"
        */
        return "This is reply msg";
    }


}
