package com.awesomeinnovators.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MessageAdaper extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private String TAG = "MessageAdapter";

    class ViewHolder0 extends RecyclerView.ViewHolder {
        View their_avatar;
        TextView their_name,message_body;

        public ViewHolder0(@NonNull View itemView) {
            super(itemView);
            their_avatar = (View) itemView.findViewById(R.id.their_avatar);
            their_name = (TextView) itemView.findViewById(R.id.their_name);
            message_body = (TextView) itemView.findViewById(R.id.message_body);
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {
        TextView message_body;

        public ViewHolder2(@NonNull View itemView) {
            super(itemView);
            message_body = (TextView) itemView.findViewById(R.id.message_body);
        }
    }


    List<Message> list = Collections.emptyList();
    Context context;

    /* Constructor */
    public MessageAdaper(List<Message> list, Context context) {
        this.list = list;
        this.context = context;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        switch (i) {
            case 0: {
                Log.i(TAG, "if executed :: onCreateViewHolder " + isTheirMsg(i) + " position : " + i );

                Context context = viewGroup.getContext();
                LayoutInflater inflater = LayoutInflater.from(context);

                View theirMessageView = inflater.inflate(R.layout.their_message, viewGroup, false);
                ViewHolder0 viewHolder = new ViewHolder0(theirMessageView);
                return viewHolder;

            }
            case 1: {
                Log.i(TAG, "else executed :: onCreateViewHolder ");

                Context context = viewGroup.getContext();
                LayoutInflater inflater = LayoutInflater.from(context);

                View myMessageView = inflater.inflate(R.layout.my_message, viewGroup, false);
                ViewHolder2 viewHolder2 = new ViewHolder2(myMessageView);
                return viewHolder2;
            }
        }

//        if( isTheirMsg(i) ) {
//        } else if( isMyMsg(i) ) {
//        } else {
//            /* to be handled later */
//        }



        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        if( viewHolder  instanceof ViewHolder0 ) {
            Log.i(TAG, "instanceof viewHolder 0 :: onBindViewHolder " + " position : " + i);
            ViewHolder0 viewHolder0 = (ViewHolder0)viewHolder;
            viewHolder0.message_body.setText(getMessageContent(i) + i);
            viewHolder0.their_name.setText("their Name :" + i);

        } else if( viewHolder  instanceof ViewHolder2 ) {
            Log.i(TAG, "instanceof viewHolder 2 :: onBindViewHolder " + " position : " + i);
            ViewHolder2 viewHolder2 = (ViewHolder2) viewHolder;
            viewHolder2.message_body.setText(getMessageContent(i) + i);

        } else {
            /* to be handled later */
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(final int position) {
        boolean a = list.get(position).isTheirMsg();
        int v;

        if (a == true) {
            v = 1;
        } else {
            v = 0;
        }
        return v;
    }




    public String getMessageContent(int position) {
        /* Note for my-msg and their-msg ... same field msg is used to store value */
        return list.get(position).getMessage();
    }

    public boolean isTheirMsg(int position) {
        return list.get(position).isTheirMsg();
    }
    public boolean isMyMsg(int position) {
        return list.get(position).isMyMsg();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
    }


//    private List<Message> getData()
//    {
//        List<Message> list = new ArrayList<>();
//        list.add(new Message("Message 1 ", "message id here ....asasa", false, true));
//        list.add(new Message("Message 2 ", "message id here ....asasa", false, true));
//        list.add(new Message("Message 3 ", "message id here ....asasa", false, true));
//
//        return list;
//    }
}
