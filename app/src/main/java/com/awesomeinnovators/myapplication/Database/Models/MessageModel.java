package com.awesomeinnovators.myapplication.Database.Models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class MessageModel {
    public static final String DATABASE_NAME        = "NeetDatabase";       // Database Name
    public static final String TABLE_NAME           = "messages";               // Table Name
    public static final int    DATABASE_Version     = 1;                    // Database Version
    public static final String UID                  = "_id";                // Column I (Primary Key)
    public static final String Message_Content      = "Guest";              //Column II
    public static final String Sended           = "Password";           // Column III
    public static final String Recieved                = "Password";          // Column IV
    public static final String Reply_on               = "Password";            // Column V
    public static final String updated_at             = "Password";            // Column VI
    public static final String TAG                      = "MessageModel";


    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+
            " ("
            + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + Message_Content + " VARCHAR(2000) ,"
            + Sended +" VARCHAR(10), "
            + Recieved +" VARCHAR(10), "
            + Reply_on +" VARCHAR(256), "
            + updated_at + " VARCHAR(256) "
            + ");";

    public static final String DROP_TABLE ="DROP TABLE IF EXISTS "+TABLE_NAME;

    public static void message(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.i(TAG, message + "");
    }
}
