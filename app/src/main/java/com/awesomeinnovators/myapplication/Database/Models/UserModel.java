package com.awesomeinnovators.myapplication.Database.Models;

import android.content.Context;
import android.widget.Toast;

public class UserModel {

    public static final String DATABASE_NAME        = "NeetDatabase";       // Database Name
    public static final String TABLE_NAME           = "user";               // Table Name
    public static final int    DATABASE_Version     = 1;                    // Database Version
    public static final String UID                  = "_id";                // Column I (Primary Key)
    public static final String NAME                 = "Guest";              //Column II
    public static final String MyPASSWORD           = "Password";           // Column III
    public static final String Email                = "Password";          // Column IV
    public static final String UserID               = "Password";            // Column V
    public static final String Token                = "Password";            // Column VI
    public static final String Token_max_valid      = "Password";           // Column VII, Date

    public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+
            " ("
            + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + NAME + " VARCHAR(512) ,"
            + MyPASSWORD +" VARCHAR(512), "
            + Email +" VARCHAR(512), "
            + UserID +" VARCHAR(512), "
            + Token + " VARCHAR(1024) "
            + ");";

    public static final String DROP_TABLE ="DROP TABLE IF EXISTS "+TABLE_NAME;
    
    public static void message(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
