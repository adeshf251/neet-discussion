package com.awesomeinnovators.myapplication.Database.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.awesomeinnovators.myapplication.Database.Models.UserModel;

public class UserController  extends SQLiteOpenHelper {

    private Context context;
    UserController myhelper;

    public UserController(Context context) {
        super(context, UserModel.DATABASE_NAME, null, UserModel.DATABASE_Version);
        this.context=context;
        myhelper = new UserController(context);
    }

    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(UserModel.CREATE_TABLE);
        } catch (Exception e) {
            UserModel.message(context,""+e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            UserModel.message(context,"OnUpgrade");
            db.execSQL(UserModel.DROP_TABLE);
            onCreate(db);
        }catch (Exception e) {
            UserModel.message(context,""+e);
        }
    }


    public long insertData(String name, String pass)
    {
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserModel.NAME, name);
        contentValues.put(UserModel.MyPASSWORD, pass);
        long id = dbb.insert(UserModel.TABLE_NAME, null , contentValues);
        return id;
    }

    public String getData()
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {UserModel.UID, UserModel.NAME, UserModel.MyPASSWORD};
        Cursor cursor =db.query(UserModel.TABLE_NAME,columns,null,null,null,null,null);
        StringBuffer buffer= new StringBuffer();
        while (cursor.moveToNext())
        {
            int cid =cursor.getInt(cursor.getColumnIndex(UserModel.UID));
            String name =cursor.getString(cursor.getColumnIndex(UserModel.NAME));
            String  password =cursor.getString(cursor.getColumnIndex(UserModel.MyPASSWORD));
            buffer.append(cid+ "   " + name + "   " + password +" \n");
        }
        return buffer.toString();
    }

    public  int delete(String uname)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] whereArgs ={uname};

        int count =db.delete(UserModel.TABLE_NAME , UserModel.NAME+" = ?",whereArgs);
        return  count;
    }

    public int updateName(String oldName , String newName)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(UserModel.NAME,newName);
        String[] whereArgs= {oldName};
        int count =db.update(UserModel.TABLE_NAME,contentValues, UserModel.NAME+" = ?",whereArgs );
        return count;
    }


}