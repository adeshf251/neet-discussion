package com.awesomeinnovators.myapplication.Database.Controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.awesomeinnovators.myapplication.Database.Models.MessageModel;

public class MessageController  extends SQLiteOpenHelper {
    private Context context;
    MessageController myhelper;

    public MessageController(Context context) {
        super(context, MessageModel.DATABASE_NAME, null, MessageModel.DATABASE_Version);
        this.context=context;
        myhelper = new MessageController(context);
    }

    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(MessageModel.CREATE_TABLE);
        } catch (Exception e) {
            MessageModel.message(context,""+e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            MessageModel.message(context,"OnUpgrade");
            db.execSQL(MessageModel.DROP_TABLE);
            onCreate(db);
        }catch (Exception e) {
            MessageModel.message(context,""+e);
        }
    }


    public long insertData(String Message_Content, boolean Sended, boolean Recieved)
    {
        SQLiteDatabase dbb = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MessageModel.Message_Content, Message_Content);
        contentValues.put(MessageModel.Sended, Sended);
        contentValues.put(MessageModel.Recieved, Recieved);
        long id = dbb.insert(MessageModel.TABLE_NAME, null , contentValues);
        return id;
    }

    public Cursor getData()
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {
                MessageModel.UID,
                MessageModel.Message_Content,
                MessageModel.Sended,
                MessageModel.Recieved,
                MessageModel.Reply_on
        };
        Cursor cursor =db.query(MessageModel.TABLE_NAME,columns,null,null,null,null,null);
//        StringBuffer buffer= new StringBuffer();
//        while (cursor.moveToNext())
//        {
//            int cid =cursor.getInt(cursor.getColumnIndex(MessageModel.UID));
//            String Message_Content =cursor.getString(cursor.getColumnIndex(MessageModel.Message_Content));
//            String  Sended =cursor.getString(cursor.getColumnIndex(MessageModel.Sended));
//            String  Recieved =cursor.getString(cursor.getColumnIndex(MessageModel.Recieved));
//            buffer.append(cid+ "   " + Message_Content + "   " + Sended + "   " + Recieved +" \n");
//        }
//        return buffer.toString();
        return cursor;
    }

    public  int delete(String id)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] whereArgs ={id};

        int count =db.delete(MessageModel.TABLE_NAME , MessageModel.UID+" = ?",whereArgs);
        return  count;
    }

    public int updateMessageContent(String oldMessage , String newMessage)
    {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MessageModel.Message_Content,newMessage);
        String[] whereArgs= {oldMessage};
        int count =db.update(MessageModel.TABLE_NAME,contentValues, MessageModel.Message_Content+" = ?",whereArgs );
        return count;
    }

    public Cursor getLast25Items() {
        SQLiteDatabase db = myhelper.getWritableDatabase();
        String[] columns = {
                MessageModel.UID,
                MessageModel.Message_Content,
                MessageModel.Sended,
                MessageModel.Recieved,
                MessageModel.Reply_on
        };
        Cursor cursor =db.query(
                MessageModel.TABLE_NAME,columns,
                null,
                null,
                null,
                null,
                null
        );
        return cursor;
    }

    public Cursor getPervious25Items() {
        return null;
    }

    public Cursor getNext25Items() {
        return null;
    }

    public int getMessageCategory() {
        /* use category instead of sender, reciever, other
        * 1- Sended
        * 3- Recieved
        * 5- group
        * 7-broadcasted
        * 9- Important
        * 11- Admin Notification
        * */
        return 0;
    }


}